import json
import pickle

import spotipy
from spotipy.oauth2 import SpotifyClientCredentials
from tqdm import tqdm

def sanity_check_result(artist_wanted, album_wanted, proposition):
    album = proposition['name']
    names = [(x['name'], x['uri']) for x in proposition["artists"]]
    check_name = [x for x in names if artist_wanted.lower() in x[0].lower()]
    check_album = album_wanted.lower() in album.lower()
    if check_name and check_album:
        return check_name[0][1], proposition['uri']
    else:
        return None, None

def album_id_to_tracks(id):
    album_tracks = []
    album_tracks_pages = sp.album_tracks(id)
    album_tracks.extend(album_tracks_pages['items'])
    while album_tracks_pages['next']:
        album_tracks_pages = sp.next(album_tracks_pages)
        album_tracks.extend(album_tracks_pages['items'])

def build_extracted_track(x, album_string, artist):
    d = {k: x[k] for k in ['name', 'disc_number', 'track_number', 'uri']}
    d['album'] = album_string
    d['artist'] = artist_string
    d['release_date_album'] = search_result['release_date']
    d['album_uri'] = album_uri
    d['artist_uri'] = artist_uri

if __name__ == "__main__":
    nb_successful_retrieval = 0
    nb_requests = 0
    audio_features_limit = 50
    sep = "\t"

    with open('albums_artists_shuffled.pkl', 'rb') as f:
        to_do = pickle.load(f)

    client_credentials_manager = SpotifyClientCredentials(client_id='XXXX',
                                                          client_secret='XXXX')
    sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager)
    sp.trace = False

    audio_features = []
    extracted_tracks = []
    uris = []
    file_number = 0
    nb_errors = 0

    for artist_string, album_string in tqdm(to_do):
        try:
            search_result = \
            sp.search(q='artist:' + artist_string + ' album:' + album_string, limit=1, type='album')['albums']['items']
            nb_requests += 1
            if search_result:
                search_result = search_result[0]
                artist_uri, album_uri = sanity_check_result(artist_string, album_string, search_result)
                if artist_uri and album_uri:
                    nb_successful_retrieval += 1

                    album_tracks = album_id_to_tracks(search_result['id'])

                    for x in album_tracks:
                        d = {k: x[k] for k in ['name', 'disc_number', 'track_number', 'uri'],}
                        d['album'] = album_string
                        d['artist'] = artist_string
                        d['release_date_album'] = search_result['release_date']
                        d['album_uri'] = album_uri
                        d['artist_uri'] = artist_uri
                        uris.append(x['uri'])
                        if len(uris) == audio_features_limit:
                            audio_features.extend(sp.audio_features(uris))
                            uris = []
                        extracted_tracks.append(d)
        except:
            print("error with artist: %s and album %s" % (artist_string, album_string))
            nb_errors += 1
            pass

        if len(extracted_tracks) > 2000:
            with open('../data/audio_features/audio_features%04d' % file_number, 'w') as f1, \
                    open('../data/audio_features/extracted_tracks%04d' % file_number, 'w') as f2:
                for k in audio_features:
                    json.dump(k, f1)
                    f1.write('\n')
                for k in extracted_tracks:
                    json.dump(k, f)
                    f.write('\n')
            file_number += 1
            audio_features = []
            extracted_tracks = []

    print("nb requests: %d nb succesful retrieval: %d nb errors: %d" % (nb_requests, nb_successful_retrieval, nb_errors))