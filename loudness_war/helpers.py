from socket import timeout
from urllib.request import Request, urlopen
from datetime import datetime
from tqdm import tqdm
from dateutil import rrule
from urllib.error import URLError, HTTPError
try:
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup

def get_range(date1, date2=None, mode=rrule.WEEKLY, fmt='%Y-%m-%d'):
    """Return a list of dates between date1 and date2 with delta specified by mode"""
    if date2 is None:
        date2 = datetime.now().strftime(fmt)
    return [dt.strftime(fmt) for dt in rrule.rrule(mode,
                                  dtstart=datetime.strptime(date1, fmt),
                                  until=datetime.strptime(date2, fmt))]

def get_html_content(url, max_attempt = 3):
    req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})

    for attempt in range(max_attempt):
        try:
            return urlopen(req, timeout=10).read()
        except HTTPError as e:
            print("The server couldn\'t fulfill the request....attempt %d/%d" % (attempt + 1, max_attempt))
            print('Error code: ', e.code)
        except URLError as e:
            print("We failed to reach a server....attempt %d/%d" % (attempt + 1, max_attempt))
            print('Reason: ', e.reason)
        except timeout as e:
            print('timeout...attempt %d/%d' % (attempt + 1, max_attempt))

def parse_html(html_content, sep = "\t"):
    res = []
    soup = BeautifulSoup(html_content, "html.parser")
    artist_n1, album_n1 = soup.select("div.chart-number-one__artist")[0].text.strip(), \
                            soup.select("div.chart-number-one__title")[0].text.strip()
    res.append(sep.join((artist_n1,album_n1,"1")))
    for e in soup.select('div.chart-list-item'):
        artist, album, rank = e.get("data-artist", "unknown"), \
                              e.get("data-title", "unknown"), \
                              e.get("data-rank", "unknown")
        res.append(sep.join((artist, album, rank)))
    return res

def main():

    date1 = "1963-08-17"
    to_write = {}
    chunk_size = 50
    idx = 0
    file_number = 1
    dump = False
    for d in tqdm(get_range(date1)):
        idx += 1
        url = 'https://www.billboard.com/charts/billboard-200/' + d
        html_content = get_html_content(url)
        if html_content:
            to_write[d] = parse_html(html_content)
        else:
            dump = True

        if idx % chunk_size == 0 or dump:
            with open('../data/billboard200/charts_data%04d'%file_number, 'w') as f:
                for k, v in to_write.items():
                    for item in v:
                        f.write("%s\t%s\n" % (item, k))
            if dump:
                raise RuntimeError("Something went wrong")
            file_number += 1
            to_write = {}


if __name__ == "__main__":
    main()
